package main;

public class Main {
	public static void main(String[] args) {
String example = "This is a practice, no worries";
		
		char ch1 = example.charAt(0);
		char ch2 = example.charAt(10);
		//char ch3 = example.charAt(50);-> out of range
		
		System.out.println("The character at 0 index: " + ch1);
		System.out.println("The character at 10th index: " + ch2);
	}
}
